# Grit and Growth Mindset

### 1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.

Grit mindset as sustained passion and perseverance is a crucial factor in acheving long-term success. It is more than talent or intelligence.

### 2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.

The concept of locus of control and its impact on motivation to achive goals. An external locus of control fell their outcomes decreases motivation.

### 3. What is the Internal Locus of Control? What is the key point in the video?

The internal locus of control refers to the beleif one can influence their outcomes and events through actions. The key point in this video is cultivating locus of control and boost motivation and success.

### 4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).

- Learn from others
- Question your assumptions
- Keep trying to improve
- Use feedbacks to improve
- Take challenges to learn and grow
- Focus on hard work

### 5. What are your ideas to take action and build Growth Mindset?

- Putting Effort to grow and learn
- Taking challenges
- Learn from mistakes
- Taking feedback to work on it
- Focus on hard work to learn and grow
- Understanding each concept clearly


