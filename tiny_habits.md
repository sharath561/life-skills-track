### 1. what was the most interesting story or idea for you?
- The most interesting part is Dr. BJ Fogg's concept that doing small things after any of our routine activities makes those tiny habits habitual.

### 2. How can you use B = MAP to make making new habits easier? What are M, A and P

- B = MAP stands for Behaviour = Motivation + Ability + Prompt. To make new habits easier.

- Motivation (M): Increase you motivation to start the tasks or to perform the habit.
- Ability (A): Ability to do the task and make easier.
- Prompt (P): Use a clear cue to remind you to perform the habit.

### 3. Why it is important to "Shine" or Celebrate after each successful completion of habit?

- Celebrating after completing each habit fills us with pride and also helps to motivate us to continue with the next tasks.

### 4. what was the most interesting story or idea for you?
- The most interesting idea is James Clear's concept of "identity-based habits", which focuses on changing your habits by becoming the person you want to be, rather than just achieving specific goals.

### 5. What is the book's perspective about Identity?

- The book's perspective about Identity is the ultimate form of intrinsic motivation is when you create habits that align with that identity, leading to more sustainable behaviour change.

### 6.Write about the book's perspective on how to make a habit easier to do?
To make a habit easier:
- Start the habit
- Motivating to do tasks or habit
- The action or habit we perform
- Reward the end goal.

### 7. Write about the book's perspective on how to make a habit harder to do?
To make a habit harder:
- Increase physical effort
- Change environment
- Making it attractive
- Making it easy to do the task
- Get support

### 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- The one habit that I would like to do more of is to woke up early and to sleep early after completing the task. I will schedule my plan clearly and acheive that tasks daily.

### 9. Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
The one habit that I would like to eliminate is negative thinking. Whenever I hear any news, my mind automatically starts thinking negatively and overthinking about it. This makes my day sad and boring, and I feel down.

- Think positively
- Practice meditation
- Control negative thoughts
 