## What is the Feynman Technique?
Feynman Technique says that while we learning something make a good note in simple words that we understand and we can able to explain them to others clearly about the topic.

## In this video, what was the most interesting story or idea for you?

The most interesting idea for me in this video is the Pamadora Technique. 
- While doing any task set the timer to 25 minutes.
- Take a small gap of 5 minutes rest.
- Back to work of 25 minutes.
- Repeat the process

## What are active and diffused modes of thinking?

- In the active mode of thinking we have more focus on the work and concentration.
- In diffused mode, we will get good ideas for solving the problem

## According to the video, what are the steps to take when approaching a new topic?

- Deconstruct the skill.
- Learn enough to self-correct.
- Remove practice barriers
- Practice at least 20 hours

## What are some of the actions you can take going forward to improve your learning process?

- Pamadora Technique.
- Avoid distractions.
- Focusing on the task.
- Taking short notes while learning.
- Able to explain the topic to others.
- Learn enough to self-correct.
- Consistency in learning and practicing


