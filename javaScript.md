# <center>JavaScript</center>

JavaScript is a dynamic programming language that's used for web development, in web applications, for game development, and lots more. It allows you to implement dynamic features on web pages that cannot be done with only HTML and CSS.

## Data Types

JavaScript includes primitive and non-primitive data types.

The primitive data types in JavaScript includes :

 - string
 - number
 - boolean
 - undefined
 - null
 - NaN
 
The non-primitive data type includes:

- object
- array

## Primitive Data Types

### String

Strings can be created in javascript by using double quotes (""),single quotes (''),and by using backticks(``).The type of quote must be used on the both sides.The typeOf return value is string.

```javascript
const sample = "Hii";
console.log(sample) // Hii
console.log(typeof "JavaScript") // string
```

### Number

The Number type id capable of storing the positive and negative numbers in the safe range of -(2<sup>53</sup> -1) to (2<sup>53</sup> -1).The typeOf return value of a Number is number.

```javascript
const sample = 61;
console.log(sample) // 61
console.log(typeof sample) // number
```

### Boolean

The Boolean represents the logical entity with two values,they are : 
 - true
 - false

The typeOf return value of Bollean is boolean

```javascript
const sample = true
console.log(sample) // true
console.log(typeof sample) // boolean
```

### Undefined

In javaScript undefined is a special value it signifies that absence of the value and when function paramters are not provided a value and if the object is not provieded it will give undefined.The return typeOf undefined is undefined

```javascript
let sample;
console.log(sample) // undefined
console.log(typeof sample) //undefined
```

### Null

In javaScript null is a special value that represents when the absence of value in object.TypeOf return value of null is object

```javascript
let sample = null
console.log(sample) // null
console.log(typeof null) //object
```

### NaN

NaN stands for Not a Number.It is a special value in javaScript representing an undefined numerical result.The typeOf return value is number

``` javascript
let result = 0/0
console.log(result) // NaN
console.log(typeof NaN) // number
```

## Non - primitive Data Types

### Array

In javaScript array is a special object used for storing the mutiple values in a single variable.It also provide us to manipulate the data by adding,deleting,updating the values in the array.The typeOf return value is object

```javascript
let array = [1,2,3] // creation
array[0] = 4 // updating
array.push(5) // adding
array.pop() // deleting
console.log(typeof array) // object
```
### Object

In javaScript the objects are used for the store the entities in the form of key and value pair.The keys are in the form of string and values may be any data type .It may be functions and also other object.It provide a way to store and manipulate the data.

```javascript
let objectStore = {
    name : 'Bruce Wayne'
    character : 'BatMan' // creation
    age ;'32'
}
objectStore["color"] = "black" // adding
objectStore["name"] = "Wayne" // updating
delete objectStore["age"] // deleteing
objectStore["increment"] = function (){ // function as a value
    console.log("called increment") 
}  
objectStore["array"] = [1,2,3,4] // array as a value
console.log(typeof objectStore) // object
```

## Basic Data structures in JavaScript

- Arrays
- Objects
- Sets
- Maps

### Arrays

In javaScript array is a special object used for storing the mutiple values in a single variable.It also provide us to manipulate the data by adding,deleting,updating the values in the array.The typeOf return value is object

```javascript
let array = [1,2,3,"hii","bye"] // creation
array[0] = 4  // updating at index
array.push(5) // adding at the end
array.pop()  // deleting end element
array.shift() // removes first element
array.unshift("Hello") // add the element at the start of array
array.includes("hii") // finds the element in the array return true
array.reverse() // reverse the array
array.sort() // sort the array in ascending order
console.log(array.length)// gives the length of the array
console.log(typeof array)// object
```

### Objects

In javaScript the objects are used for the store the entities in the form of key and value pair.The keys are in the form of string and values may be any data type .It may be functions and also other object.It provide a way to store and manipulate the data.

```javascript
let objectStore = {
    name : 'Bruce Wayne'
    character : 'BatMan' // creation
    age ;'32'
}
objectStore["color"] = "black" // adding
objectStore["name"] = "Wayne" // updating
delete objectStore["age"]  // deleteing
objectStore["increment"] = function (){
    console.log("called increment") // function as a value
}  
objectStore["array"] = [1,2,3,4] // array as a value
objectstore["object"] = {
    Address : "Gauthom",  //object as a value
    City : New York
}
console.log(objectStore["object"]["Address"]) // accessing the object of object
console.log(typeof objectStore) // object
```
### Sets

Sets in javascript is a type of collection of data it stores omly the unique value of elements in the set.Unlike arrays sets doesnot allows the duplicate values in the set.Sets are particularly useful when you need to store a collection of items and automatically handle duplicate values.The typeof return value of set is object.


``` javascript
let jsSet = new Set(); //creation
let  jsSetValues = new Set([1, 2, 3, 4, 5]); //storing the values in set
jsSetValues.add(8); //adding values to the set
jsSetValues.delete(1) // deleting the values from set
jsSetValues.has(3) // finding the element in sets
console.log(typeof jsSet) // returns object
```

### Maps

Maps in javaScript is a special type of collection of data where it stores the data in the form of key and value pairs.where both keys and values can be of any type. Maps are particularly useful when you need to associate data with keys, and the keys are not limited to strings. Unlike objects, maps maintain the order of their elements based on the insertion order

```javascript
let jsMap = new Map(); //creation of Map
let myMap = new Map([
    ["key1", "value1"], //adding keys and values to Map
    ["key2", "value2"]
]);
myMap.set("key3", "value3");
myMap.set("key4", "value4"); //adding keys and values to Mao using set
console.log(myMap.get("key1")); // accessing the values from Map
myMap.delete("key1"); //deleting the values from the Map
myMap.clear();// clearing all the data in the Map
console.log(typeof jsMap) // returns Object
```
### References

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures
- https://www.geeksforgeeks.org/primitive-and-non-primitive-data-types-in-javascript/
- https://www.youtube.com/watch?v=sRpW6SFlZrQ





















